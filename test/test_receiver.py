#!/usr/bin/env python3

import os
import time
import json
import httpx
import logging
import random
import asyncio
import unittest
from unittest import mock
from functools import partial

import traceback

from receiver import MoonbounceReceiver
from moonbounce_api_client import MoonbounceAPIClient


class TestReceiver(unittest.IsolatedAsyncioTestCase):

    receiver_client_cofnig = {
        'host_url': 'http://127.0.0.1',
        'api_route': 'api',
        'port': 5000,
        'api_version': '1.0.1',
        'session_id': 0,
        'client_type': 'receiver',
        'client_id': '[rx_uuid]'}

    example_job = {'id': 1001,
                   'session': 0,
                   'clients': ['receiver', 'transmitter'],
                   'action': 'new_transmission',
                   'job_state': 'started',
                   'receiver_state': '',
                   'receiver_error': '',
                   'receiver_completed': False,
                   'transmitter_state': '',
                   'transmitter_error': '',
                   'transmitter_completed': False,
                   'transmission_start_time': '',
                   'transmission_end_time': '',
                   'transmission_duration': 0,
                   'audio_file': 'audio.wav',
                   }

    halt_command = {'id': 2005,
                    'session': 0,
                    'action': 'halt',
                    'clients': ['receiver']
                    }
    start_command = {'id': 2006,
                     'session': 0,
                     'action': 'start',
                     'clients': ['receiver', 'transmitter'],
                     'receiver_active': False,
                     'transmitter_active': False,
                     }

    mock_calls = []

    def setUp(self):

        random.seed()
        receiver_api_client = MoonbounceAPIClient(self.receiver_client_cofnig)
        self.receiver = MoonbounceReceiver(receiver_api_client)

    def get_random_state_id(self, halt_available=True, idle_available=True):
        while True:
            new_state_name = random.choice(self.receiver.rx_states)
            if new_state_name == 'halt':
                if halt_available is True:
                    break
            elif new_state_name == 'idle':
                if idle_available is True:
                    break
            else:
                break

        new_state_id = getattr(self.receiver.receiver_state, new_state_name)
        return new_state_id

    def test_receiver_state_change(self):

        new_state_name = random.choice(self.receiver.rx_states)
        new_state_id = getattr(self.receiver.receiver_state, new_state_name)
        self.receiver.receiver_state_change(new_state_id)
        self.assertTrue(self.receiver.receiver_current_state == new_state_id)

        '''
        State can not be changed when the current state is 'halt'
        '''
        previous_state = self.receiver.receiver_current_state
        self.receiver.receiver_state_change(self.receiver.receiver_state.halt)
        new_state_id = get_random_state_id(halt_available=False)
        self.receiver.receiver_state_change(new_state_id)
        self.assertFalse(self.receiver.receiver_current_state == new_state_id)
        self.assertTrue(self.receiver.receiver_current_state
                        == self.receiver.receiver_state.halt)
        '''
        State can be __force__ to be changed even in 'halt' state
        '''
        new_state_id = get_random_state_id(halt_available=False)
        self.receiver.receiver_state_change(new_state_id, force=False)
        self.assertFalse(self.receiver.receiver_current_state == new_state_id)
        self.receiver.receiver_state_change(new_state_id, force=True)
        self.assertTrue(self.receiver.receiver_current_state == new_state_id)

    def test_receiver_state_go_idle(self):
        '''
        change the current state if the current state is 'halt'
        '''
        if self.receiver.receiver_current_state == self.receiver.receiver_state.halt:
            new_state_id = get_random_state_id(halt_available=False)
            self.receiver.receiver_state_change(new_state_id, force=True)

        self.receiver.receiver_state_go_idle()
        self.assertTrue(self.receiver.receiver_current_state
                        == self.receiver.receiver_state.idle)

        self.receiver.receiver_state_change(self.receiver.receiver_state.halt)
        self.receiver.receiver_state_go_idle()
        self.assertFalse(self.receiver.receiver_current_state
                         == self.receiver.receiver_state.idle)
        self.assertTrue(self.receiver.receiver_current_state
                        == self.receiver.receiver_state.halt)
        '''
        State can be __force__ to be changed to idle even in 'halt' state
        '''
        self.receiver.receiver_state_go_idle(force=False)
        self.assertFalse(self.receiver.receiver_current_state
                         == self.receiver.receiver_state.idle)
        self.receiver.receiver_state_go_idle(force=True)
        self.assertTrue(self.receiver.receiver_current_state
                        == self.receiver.receiver_state.idle)

    async def mock_receiver_report_to_server(mock_calls, message, job_id):
        message.update({'job_id': job_id})
        mock_calls.append(message)

    @mock.patch('receiver.MoonbounceReceiver.receiver_report_to_server',
                new=partial(mock_receiver_report_to_server, mock_calls))
    async def test_state_idle_director(self):
        new_job = self.example_job.copy()
        self.mock_calls.clear()
        self.receiver.receiver_state_go_idle(force=True)

        '''normal route'''
        await self.receiver.state_idle_director(new_job)
        self.assertIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[0]['updates']['receiver_state'],
                         'record_starting')
        self.assertEqual(self.receiver.receiver_current_job_id, new_job['id'])

        self.assertIn('updates', self.mock_calls[1])
        self.assertEqual(self.mock_calls[1]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[1]['updates']['receiver_state'],
                         'recording')

        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.recording)
        self.assertEqual(self.receiver.receiver_state_max_time, 20)

        self.mock_calls.clear()

        '''transmission has already ended'''
        self.receiver.receiver_state_go_idle(force=True)
        new_job['transmission_end_time'] = time.time()
        await self.receiver.state_idle_director(new_job)
        self.assertIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[0]['updates']['receiver_state'],
                         'idle')
        self.assertEqual(
            self.mock_calls[0]['updates']['receiver_error']['type'],
            'transmission was ended')

        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.idle)
        self.assertEqual(self.receiver.receiver_current_job_id, None)
        self.assertEqual(self.receiver.receiver_state_max_time, -1)

    @mock.patch('receiver.MoonbounceReceiver.receiver_report_to_server',
                new=partial(mock_receiver_report_to_server, mock_calls))
    async def test_state_recording_director(self):
        new_job = self.example_job.copy()
        self.mock_calls.clear()
        self.receiver.receiver_state_change(
            self.receiver.receiver_state.recording, force=True)
        self.receiver.receiver_current_job_id = new_job['id']

        '''transmission has not been started'''
        await self.receiver.state_recording_director(new_job)
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.recording)
        self.assertEqual(self.receiver.receiver_current_job_id, new_job['id'])
        self.assertEqual(self.receiver.receiver_state_max_time, 20)
        self.mock_calls.clear()

        '''transmission has been started but not ended'''
        new_job['transmitter_state'] = 'transmitting'
        new_job['transmission_start_time'] = time.time()
        new_job['transmission_duration'] = 7
        await self.receiver.state_recording_director(new_job)
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.recording)
        self.assertEqual(self.receiver.receiver_current_job_id, new_job['id'])
        self.assertEqual(self.receiver.transmission_start_time,
                         new_job['transmission_start_time'])
        self.assertEqual(self.receiver.transmission_duration,
                         new_job['transmission_duration'])
        self.assertEqual(self.receiver.receiver_state_max_time,
                         new_job['transmission_duration'] + 3)
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.recording)
        self.mock_calls.clear()

        for i in range(2):
            if i == 0:
                '''transmission has ended'''
                new_job['transmission_end_time'] = time.time()
                new_job['transmitter_state'] = 'transmit_ended'
            elif i == 1:
                '''transmission has ended and has gone to idle before receiver polling'''
                self.receiver.receiver_state_change(
                    self.receiver.receiver_state.recording, force=True)
                new_job['transmitter_state'] = 'idle'
                new_job['transmitter_completed'] = True
                self.receiver.transmission_end_time = ""

            await self.receiver.state_recording_director(new_job)
            self.assertEqual(self.receiver.transmission_end_time,
                             new_job['transmission_end_time'])
            self.assertIn('updates', self.mock_calls[0])
            self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
            self.assertEqual(self.mock_calls[0]['updates']['receiver_state'],
                             'recording_stopped')

            self.assertEqual(self.mock_calls[1]['job_id'], new_job['id'])
            self.assertEqual(self.mock_calls[1]['updates']['receiver_state'],
                             'sending_audio_file')

            self.assertIn('audio_clip_sending', self.receiver.background_task)
            self.assertEqual(self.receiver.receiver_state_max_time, 20)
            self.assertEqual(self.receiver.receiver_current_state,
                             self.receiver.receiver_state.sending_audio_file)

            await self.receiver.background_task['audio_clip_sending']
            self.assertEqual(self.receiver.receiver_current_state,
                             self.receiver.receiver_state.audio_file_sent)
            self.assertEqual(self.receiver.receiver_state_max_time, 10)

            self.mock_calls.clear()

    @mock.patch('receiver.MoonbounceReceiver.receiver_report_to_server',
                new=partial(mock_receiver_report_to_server, mock_calls))
    async def test_state_audio_file_sent_director(self):
        self.mock_calls.clear()
        self.receiver.receiver_state_change(
            self.receiver.receiver_state.audio_file_sent, force=True)
        new_job = self.example_job.copy()
        self.receiver.background_task['audio_clip_sending'] \
            = self.receiver.receiver_send_audio('uri_to_audio')
        await self.receiver.state_audio_file_sent_director(new_job)

        self.assertIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[0]['updates']['receiver_state'],
                         'idle')
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.idle)
        self.assertEqual(self.receiver.receiver_state_max_time, -1)
        self.assertEqual(self.receiver.receiver_current_job_id, None)
        self.mock_calls.clear()

        '''if there is 'halt' command when the audio file is sending'''
        self.receiver.receiver_state_change(
            self.receiver.receiver_state.audio_file_sent, force=True)
        self.receiver.receiver_current_job_id = new_job['id']
        self.receiver.background_task['audio_clip_sending'] \
            = self.receiver.receiver_send_audio('uri_to_audio')
        self.receiver.receiver_state_change(
            self.receiver.receiver_state.halt, -1)
        await self.receiver.state_audio_file_sent_director(new_job)

        self.assertEqual(self.mock_calls, [])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.halt)
        self.assertEqual(self.receiver.receiver_state_max_time, -1)
        self.assertEqual(self.receiver.receiver_current_job_id, new_job['id'])

    @mock.patch('receiver.MoonbounceReceiver.receiver_report_to_server',
                new=partial(mock_receiver_report_to_server, mock_calls))
    async def test_receiver_interprete_job(self):

        '''normal_route'''
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        self.receiver.receiver_state_go_idle(force=True)
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.recording)
        self.assertEqual(self.receiver.receiver_current_job_id, new_job['id'])
        self.assertEqual(len(self.mock_calls), 2)

        '''session does not match.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id()
        self.receiver.receiver_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['session'] = 1
        current_job_id = self.receiver.receiver_current_job_id
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state, new_state_id)
        self.assertEqual(self.receiver.receiver_current_job_id, current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''client type does not match.'''
        new_state_id = self.get_random_state_id()
        self.receiver.receiver_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['clients'] = 'transmitter'
        current_job_id = self.receiver.receiver_current_job_id
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state, new_state_id)
        self.assertEqual(self.receiver.receiver_current_job_id, current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''the job is a 'start' command.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id()
        self.receiver.receiver_state_change(new_state_id, force=True)
        new_job = self.start_command.copy()
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.idle)
        self.assertIsNone(self.receiver.receiver_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)

        self.assertNotIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])

        '''the job is a 'halt' command.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id(halt_available=False)
        self.receiver.receiver_state_change(new_state_id, force=True)
        new_job = self.halt_command.copy()
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.halt)
        self.assertIsNone(self.receiver.receiver_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertEqual(self.receiver.receiver_state_max_time, -1)

        self.assertNotIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])

        new_state_id = self.get_random_state_id(halt_available=False)
        self.receiver.receiver_state_change(new_state_id)
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.halt)

        self.mock_calls.clear()
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.halt)
        self.assertEqual(len(self.mock_calls), 0)

        '''recovery from the 'halt' command with 'start' command'''
        self.mock_calls.clear()
        new_job = self.start_command.copy()
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.idle)
        self.assertIsNone(self.receiver.receiver_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.receiver.receiver_state_max_time, -1)

        '''the job has incorrect states or has been completed'''
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        new_job['job_state'] = 'waiting'
        current_job_id = self.receiver.receiver_current_job_id
        current_state = self.receiver.receiver_current_state
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state, current_state)
        self.assertEqual(self.receiver.receiver_current_job_id, current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        for i in range(3):
            self.mock_calls.clear()
            new_job = self.example_job.copy()
            if i == 0:
                new_job['job_state'] = 'waiting'
            elif i == 1:
                new_job['receiver_completed'] = True
            elif i == 2:
                new_job['receiver_error'] = 'timeout'

            current_job_id = self.receiver.receiver_current_job_id
            current_state = self.receiver.receiver_current_state
            await self.receiver.receiver_interprete_job([new_job,])
            self.assertEqual(
                self.receiver.receiver_current_state,
                current_state)
            self.assertEqual(
                self.receiver.receiver_current_job_id,
                current_job_id)
            self.assertEqual(len(self.mock_calls), 0)

        '''action is not 'new_transmission'.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.receiver.receiver_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['action'] = 'no_opt'
        current_job_id = self.receiver.receiver_current_job_id
        await self.receiver.receiver_interprete_job([new_job,])
        self.assertEqual(self.receiver.receiver_current_state, new_state_id)
        self.assertEqual(self.receiver.receiver_current_job_id, current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

    @mock.patch('receiver.MoonbounceReceiver.receiver_report_to_server',
                new=partial(mock_receiver_report_to_server, mock_calls))
    async def test_check_receiver_state_timeout(self):

        '''no timeout'''
        self.mock_calls.clear()
        current_job_id = 1001
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.receiver.receiver_state_change(new_state_id, 5)
        self.receiver.receiver_current_job_id = current_job_id
        await asyncio.sleep(3)
        await self.receiver.check_receiver_state_timeout()
        self.assertEqual(self.receiver.receiver_current_state, new_state_id)
        self.assertEqual(self.receiver.receiver_current_job_id, current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''timeout'''
        self.mock_calls.clear()
        current_job_id = 1001
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.receiver.receiver_state_change(new_state_id, 3)
        self.receiver.receiver_current_job_id = current_job_id
        await asyncio.sleep(5)
        await self.receiver.check_receiver_state_timeout()
        self.assertEqual(self.receiver.receiver_current_state,
                         self.receiver.receiver_state.idle)
        self.assertIsNone(self.receiver.receiver_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertIn('updates', self.mock_calls[0])
        self.assertIn('receiver_error', self.mock_calls[0]['updates'])
        self.assertEqual(self.mock_calls[0]['job_id'], current_job_id)
        self.assertEqual(
            self.mock_calls[0]['updates']['receiver_state'],
            'idle')
        self.assertEqual(
            self.mock_calls[0]['updates']['receiver_error']['type'],
            'timeout')
        self.assertEqual(self.mock_calls[0]['updates']['receiver_error']
                         ['state'], self.receiver.rx_states[new_state_id - 1])

    async def asyncTearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(failfast=True)
