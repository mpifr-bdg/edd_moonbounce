#!/usr/bin/env python3

import os
import time
import json
import httpx
import logging
import random
import asyncio
import unittest
from unittest import mock
from functools import partial

import traceback

from transmitter import MoonbounceTransmitter
from moonbounce_api_client import MoonbounceAPIClient


class TestTransmitter(unittest.IsolatedAsyncioTestCase):

    transmitter_client_cofnig = {
        'host_url': 'http://127.0.0.1',
        'api_route': 'api',
        'port': 5000,
        'api_version': '1.0.1',
        'session_id': 0,
        'client_type': 'transmitter',
        'client_id': '[tx_uuid]'}

    example_job = {'id': 1001,
                   'session': 0,
                   'clients': ['receiver', 'transmitter'],
                   'action': 'new_transmission',
                   'job_state': 'started',
                   'receiver_state': '',
                   'receiver_error': '',
                   'receiver_completed': False,
                   'transmitter_state': '',
                   'transmitter_error': '',
                   'transmitter_completed': False,
                   'transmission_start_time': '',
                   'transmission_end_time': '',
                   'transmission_duration': 0,
                   'audio_file': 'audio.wav',
                   }

    halt_command = {'id': 2005,
                    'session': 0,
                    'action': 'halt',
                    'clients': ['transmitter']
                    }
    start_command = {'id': 2006,
                     'session': 0,
                     'action': 'start',
                     'clients': ['receiver', 'transmitter'],
                     'receiver_active': False,
                     'transmitter_active': False,
                     }

    mock_calls = []

    def setUp(self):

        random.seed()
        transmitter_api_client = MoonbounceAPIClient(
            self.transmitter_client_cofnig)
        self.transmitter = MoonbounceTransmitter(transmitter_api_client)

    def get_random_state_id(self, halt_available=True, idle_available=True):
        while True:
            new_state_name = random.choice(self.transmitter.tx_states)
            if new_state_name == 'halt':
                if halt_available is True:
                    break
            elif new_state_name == 'idle':
                if idle_available is True:
                    break
            else:
                break

        new_state_id = getattr(
            self.transmitter.transmitter_state,
            new_state_name)
        return new_state_id

    def test_transmitter_state_change(self):

        new_state_name = random.choice(self.transmitter.tx_states)
        new_state_id = getattr(
            self.transmitter.transmitter_state,
            new_state_name)
        self.transmitter.transmitter_state_change(new_state_id)
        self.assertTrue(
            self.transmitter.transmitter_current_state == new_state_id)

        '''
        State can NOT be changed when the current state is 'halt'
        '''
        previous_state = self.transmitter.transmitter_current_state
        self.transmitter.transmitter_state_change(
            self.transmitter.transmitter_state.halt)
        new_state_id = self.get_random_state_id(halt_available=False)
        self.transmitter.transmitter_state_change(new_state_id)
        self.assertFalse(
            self.transmitter.transmitter_current_state == new_state_id)
        self.assertTrue(self.transmitter.transmitter_current_state
                        == self.transmitter.transmitter_state.halt)
        '''
        State can be __FORCE__ to be changed even in 'halt' state
        '''

        new_state_id = self.get_random_state_id(halt_available=False)
        self.transmitter.transmitter_state_change(new_state_id, force=False)
        self.assertFalse(
            self.transmitter.transmitter_current_state == new_state_id)
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        self.assertTrue(
            self.transmitter.transmitter_current_state == new_state_id)

    def test_transmitter_state_go_idle(self):
        '''
        change the current state if the current state is 'halt'
        '''
        if (self.transmitter.transmitter_current_state
                == self.transmitter.transmitter_state.halt):

            new_state_id = self.get_random_state_id(halt_available=False)
            self.transmitter.transmitter_state_change(new_state_id, force=True)

        self.transmitter.transmitter_state_go_idle()
        self.assertTrue(self.transmitter.transmitter_current_state
                        == self.transmitter.transmitter_state.idle)

        self.transmitter.transmitter_state_change(
            self.transmitter.transmitter_state.halt)
        self.transmitter.transmitter_state_go_idle()
        self.assertFalse(self.transmitter.transmitter_current_state
                         == self.transmitter.transmitter_state.idle)
        self.assertTrue(self.transmitter.transmitter_current_state
                        == self.transmitter.transmitter_state.halt)
        '''
        State can be __force__ to be changed to idle even in 'halt' state
        '''
        self.transmitter.transmitter_state_go_idle(force=False)
        self.assertFalse(self.transmitter.transmitter_current_state
                         == self.transmitter.transmitter_state.idle)
        self.transmitter.transmitter_state_go_idle(force=True)
        self.assertTrue(self.transmitter.transmitter_current_state
                        == self.transmitter.transmitter_state.idle)

    async def mock_transmitter_report_to_server(mock_calls, message, job_id):
        message.update({'job_id': job_id})
        mock_calls.append(message)

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_transmitter_pull_audio(self):
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        for i in range(2):
            pull_task = asyncio.create_task(
                self.transmitter.transmitter_pull_audio(
                    new_job["id"], "https://audio_url"))
            if i == 0:
                '''normal route'''
                await pull_task
                self.assertEqual(len(self.mock_calls), 1)
                self.assertIn('updates', self.mock_calls[0])
                self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
                self.assertEqual(
                    self.mock_calls[0]['updates']['transmitter_state'],
                    'ready_to_transmit')
                self.assertEqual(
                    self.transmitter.transmitter_current_state,
                    self.transmitter.transmitter_state.ready_to_transmit)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, 20)

            elif i == 1:
                '''the transmitter was requested to be in 'halt' state'''
                await asyncio.sleep(0.5)
                self.transmitter.transmitter_state_change(
                    self.transmitter.transmitter_state.halt, -1)
                await pull_task

                self.assertEqual(len(self.mock_calls), 0)
                self.assertEqual(self.transmitter.transmitter_current_state,
                                 self.transmitter.transmitter_state.halt)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, -1)

            self.mock_calls.clear()

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_transmitter_transmit_radio(self):
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        current_time = time.time()
        for i in range(2):
            tx_task = asyncio.create_task(
                self.transmitter.transmitter_transmit_radio(new_job['id']))
            await asyncio.sleep(2)
            if i == 0:
                '''normal route'''
                pass
            elif i == 1:
                '''the transmitter was requested to be in 'halt' state'''
                self.transmitter.transmitter_state_change(
                    self.transmitter.transmitter_state.halt, -1)

            await tx_task
            self.assertIn('updates', self.mock_calls[0])
            self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
            self.assertEqual(
                self.mock_calls[0]['updates']['transmitter_state'],
                'transmitting')
            transmission_start_time = self.mock_calls[0]['updates'][
                'transmit_start_time']
            self.assertGreaterEqual(transmission_start_time, current_time)
            self.assertGreater(
                self.mock_calls[0]['updates']['transmission_duration'], 0)

            if i == 0:
                self.assertEqual(len(self.mock_calls), 2)
                self.assertIn('updates', self.mock_calls[1])
                self.assertEqual(self.mock_calls[1]['job_id'], new_job['id'])
                self.assertEqual(
                    self.mock_calls[1]['updates']['transmitter_state'],
                    'transmit_ended')
                self.assertGreaterEqual(
                    self.mock_calls[1]['updates']['transmission_end_time'],
                    transmission_start_time)

                self.assertEqual(
                    self.transmitter.transmitter_current_state,
                    self.transmitter.transmitter_state.transmit_ended)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, 10)

            elif i == 1:
                self.assertEqual(len(self.mock_calls), 1)
                self.assertEqual(self.transmitter.transmitter_current_state,
                                 self.transmitter.transmitter_state.halt)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, -1)

            self.mock_calls.clear()

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_state_idle_director(self):
        new_job = self.example_job.copy()
        self.mock_calls.clear()
        self.transmitter.transmitter_state_go_idle(force=True)

        '''normal route, also see the test of transmitter_pull_audio'''
        await self.transmitter.state_idle_director(new_job)
        self.assertEqual(len(self.mock_calls), 2)
        self.assertIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[0]['updates']['transmitter_state'],
                         'pulling_audio')
        self.assertIn('updates', self.mock_calls[1])
        self.assertEqual(self.mock_calls[1]['job_id'], new_job['id'])
        self.assertEqual(self.mock_calls[1]['updates']['transmitter_state'],
                         'ready_to_transmit')

        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            new_job['id'])

        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.ready_to_transmit)
        self.assertEqual(self.transmitter.transmitter_state_max_time, 20)

        self.mock_calls.clear()

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_state_ready_to_transmit_director(self):
        new_job = self.example_job.copy()
        self.mock_calls.clear()
        self.transmitter.transmitter_state_change(
            self.transmitter.transmitter_state.ready_to_transmit, force=True)
        self.transmitter.transmitter_current_job_id = new_job['id']

        '''recording has not been started'''
        await self.transmitter.state_ready_to_transmit_director(new_job)
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.ready_to_transmit)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            new_job['id'])
        self.assertEqual(self.transmitter.transmitter_state_max_time, 20)
        self.mock_calls.clear()

        '''recording has been started'''
        new_job['receiver_state'] = 'recording'
        await self.transmitter.state_ready_to_transmit_director(new_job)
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.transmitting)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            new_job['id'])
        self.assertIn('radio_transmission', self.transmitter.background_task)
        self.transmitter.background_task['radio_transmission'].cancel()
        self.transmitter.background_task['radio_transmission'] = None

        self.mock_calls.clear()

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_state_transmit_ended_director(self):
        self.mock_calls.clear()
        new_job = self.example_job.copy()

        for i in range(2):
            self.transmitter.transmitter_state_change(
                self.transmitter.transmitter_state.transmitting, force=True)
            tx_task = asyncio.create_task(
                self.transmitter.transmitter_transmit_radio(new_job['id']))
            self.transmitter.background_task['radio_transmission'] = tx_task
            state_task = self.transmitter.state_transmit_ended_director(
                new_job)

            if i == 0:
                '''normal route, see also test of transmitter_transmit_radio'''
                await state_task
                self.assertEqual(len(self.mock_calls), 3)
                self.assertIn('updates', self.mock_calls[2])
                self.assertEqual(self.mock_calls[2]['job_id'], new_job['id'])
                self.assertEqual(
                    self.mock_calls[2]['updates']['transmitter_state'], 'idle')
                self.assertTrue(
                    self.mock_calls[2]['updates']['transmitter_completed'])

                self.assertIsNone(self.transmitter.transmitter_current_job_id)

                self.assertEqual(self.transmitter.transmitter_current_state,
                                 self.transmitter.transmitter_state.idle)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, -1)

            elif i == 1:
                '''the transmitter was requested to be in 'halt' state during'''
                ''' an active transmission'''
                await asyncio.sleep(2)
                self.transmitter.transmitter_state_change(
                    self.transmitter.transmitter_state.halt, -1)
                await state_task

                self.assertEqual(len(self.mock_calls), 1)
                self.assertEqual(
                    self.mock_calls[0]['updates']['transmitter_state'],
                    'transmitting')
                self.assertEqual(self.transmitter.transmitter_current_state,
                                 self.transmitter.transmitter_state.halt)
                self.assertEqual(
                    self.transmitter.transmitter_state_max_time, -1)

            self.mock_calls.clear()

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_transmitter_interprete_job(self):

        '''normal_route, see test for idle state'''
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        self.transmitter.transmitter_state_go_idle(force=True)
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.ready_to_transmit)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            new_job['id'])
        self.assertEqual(len(self.mock_calls), 2)

        '''session does not match.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id()
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['session'] = 1
        current_job_id = self.transmitter.transmitter_current_job_id
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(
            self.transmitter.transmitter_current_state,
            new_state_id)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''client type does not match.'''
        new_state_id = self.get_random_state_id()
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['clients'] = 'receiver'
        current_job_id = self.transmitter.transmitter_current_job_id
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(
            self.transmitter.transmitter_current_state,
            new_state_id)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''the job is a 'start' command.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id()
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        new_job = self.start_command.copy()
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.idle)
        self.assertIsNone(self.transmitter.transmitter_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertNotIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])

        '''the job is a 'halt' command.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id(halt_available=False)
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        new_job = self.halt_command.copy()
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.halt)
        self.assertIsNone(self.transmitter.transmitter_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertEqual(self.transmitter.transmitter_state_max_time, -1)

        self.assertNotIn('updates', self.mock_calls[0])
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])

        new_state_id = self.get_random_state_id(halt_available=False)
        self.transmitter.transmitter_state_change(new_state_id)
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.halt)

        self.mock_calls.clear()
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.halt)
        self.assertEqual(len(self.mock_calls), 0)

        '''recovery from the 'halt' command with 'start' command'''
        self.mock_calls.clear()
        new_job = self.start_command.copy()
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.idle)
        self.assertIsNone(self.transmitter.transmitter_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertEqual(self.mock_calls[0]['job_id'], new_job['id'])
        self.assertEqual(self.transmitter.transmitter_state_max_time, -1)

        '''the job has incorrect states or has been completed'''
        self.mock_calls.clear()
        new_job = self.example_job.copy()
        new_job['job_state'] = 'waiting'
        current_job_id = self.transmitter.transmitter_current_job_id
        current_state = self.transmitter.transmitter_current_state
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(
            self.transmitter.transmitter_current_state,
            current_state)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        for i in range(3):
            self.mock_calls.clear()
            new_job = self.example_job.copy()
            if i == 0:
                new_job['job_state'] = 'waiting'
            elif i == 1:
                new_job['transmitter_completed'] = True
            elif i == 2:
                new_job['transmitter_error'] = 'timeout'

            current_job_id = self.transmitter.transmitter_current_job_id
            current_state = self.transmitter.transmitter_current_state
            await self.transmitter.transmitter_interprete_job([new_job,])
            self.assertEqual(
                self.transmitter.transmitter_current_state,
                current_state)
            self.assertEqual(
                self.transmitter.transmitter_current_job_id,
                current_job_id)
            self.assertEqual(len(self.mock_calls), 0)

        '''action is not 'new_transmission'.'''
        self.mock_calls.clear()
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.transmitter.transmitter_state_change(new_state_id, force=True)
        new_job = self.example_job.copy()
        new_job['action'] = 'no_opt'
        current_job_id = self.transmitter.transmitter_current_job_id
        await self.transmitter.transmitter_interprete_job([new_job,])
        self.assertEqual(
            self.transmitter.transmitter_current_state,
            new_state_id)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

    @mock.patch('transmitter.MoonbounceTransmitter.transmitter_report_to_server',
                new=partial(mock_transmitter_report_to_server, mock_calls))
    async def test_check_transmitter_state_timeout(self):

        '''no timeout'''
        self.mock_calls.clear()
        current_job_id = 1001
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.transmitter.transmitter_state_change(new_state_id, 5)
        self.transmitter.transmitter_current_job_id = current_job_id
        await asyncio.sleep(3)
        await self.transmitter.check_transmitter_state_timeout()
        self.assertEqual(
            self.transmitter.transmitter_current_state,
            new_state_id)
        self.assertEqual(
            self.transmitter.transmitter_current_job_id,
            current_job_id)
        self.assertEqual(len(self.mock_calls), 0)

        '''timeout'''
        self.mock_calls.clear()
        current_job_id = 1001
        new_state_id = self.get_random_state_id(
            halt_available=False, idle_available=False)
        self.transmitter.transmitter_state_change(new_state_id, 3)
        self.transmitter.transmitter_current_job_id = current_job_id
        await asyncio.sleep(5)
        await self.transmitter.check_transmitter_state_timeout()
        self.assertEqual(self.transmitter.transmitter_current_state,
                         self.transmitter.transmitter_state.idle)
        self.assertIsNone(self.transmitter.transmitter_current_job_id)
        self.assertEqual(len(self.mock_calls), 1)
        self.assertIn('updates', self.mock_calls[0])
        self.assertIn('transmitter_error', self.mock_calls[0]['updates'])
        self.assertEqual(self.mock_calls[0]['job_id'], current_job_id)
        self.assertEqual(
            self.mock_calls[0]['updates']['transmitter_state'],
            'idle')
        self.assertEqual(
            self.mock_calls[0]['updates']['transmitter_error']['type'],
            'timeout')
        self.assertEqual(self.mock_calls[0]['updates']['transmitter_error'][
                         'state'], self.transmitter.tx_states[new_state_id - 1])

    async def asyncTearDown(self):
        pass


if __name__ == '__main__':
    unittest.main(failfast=True)
