#!/usr/bin/env python3

from multiprocessing import Process
import random, time
import signal, sys
import asyncio
import unittest
from functools import partial

from transmitter import MoonbounceTransmitter
from receiver import MoonbounceReceiver
from server import MoonbounceServer
from moonbounce_api_client import MoonbounceAPIClient


random.seed()

class TestCommunication(unittest.IsolatedAsyncioTestCase):

    want_to_exit = False
    receiver_client_cofnig = {
                    'host_url': 'http://127.0.0.1',
                    'api_route': 'api',
                    'port': 5000,
                    'api_version': '1.0.1',
                    'session_id': 0,
                    'client_type': 'receiver',
                    'client_id': '[rx_uuid]'}

    transmitter_client_cofnig = {
                    'host_url': 'http://127.0.0.1',
                    'api_route': 'api',
                    'port': 5000,
                    'api_version': '1.0.1',
                    'session_id': 0,
                    'client_type': 'transmitter',
                    'client_id': '[tx_uuid]'}

    test_client_cofnig = {
                    'host_url': 'http://127.0.0.1',
                    'api_route': 'api',
                    'port': 5000,
                    'api_version': '1.0.1',
                    'session_id': 0,
                    'client_type': 'test',
                    'client_id': '[test_uuid]'}

    async def asyncTearDown(self):
        pass

    def int_signal_handler(self, process_dict, status, sig, frame):
        for key in process_dict.keys():
            process_dict[key].kill()

        status['exiting'] = True

    async def get_current_job_id(self, client_api):
        jobs =  await self.client_api.get_job(None)
        for job in jobs:
            if job['id'] < 2000 and job['job_state'] not in ['waiting', 'completed']:
                return job['id']
        else:
            return None

    async def test_unstable_receiver(self):

        unittest.installHandler()

        server = MoonbounceServer()
        receiver_api_client = MoonbounceAPIClient(self.receiver_client_cofnig)
        receiver = MoonbounceReceiver(receiver_api_client)
        transmitter_api_client = MoonbounceAPIClient(self.transmitter_client_cofnig)
        transmitter = MoonbounceTransmitter(transmitter_api_client)
        test_api_client = MoonbounceAPIClient(self.test_client_cofnig)
        server_process = Process(target=server.run)
        transmitter_process = Process(target=transmitter.run)
        receiver_process = Process(target=receiver.run)
        server_process.start()
        transmitter_process.start()
        receiver_process.start()

        status = {  'exiting': False,
                    'check_recovery': False,
                    'current_job_id': None,
                    'job_change_counter': 0,
                    'offline_timestamp':0,
                    'recover_start_timestamp':0
        }
        processes_dict = {'srv': server_process,
                          'tx': transmitter_process,
                          'rx': receiver_process}
        signal.signal(signal.SIGINT, partial(self.int_signal_handler, processes_dict, status))
        self.client_api = test_api_client
        min_running_interval = 70
        last_respawn_time = 0

        while not status['exiting']:
            '''receiver'''
            if (random.random() < 0.3
                    and receiver_process.is_alive()
                    and transmitter_process.is_alive()
                    and server_process.is_alive()
                    and time.time() - last_respawn_time > min_running_interval):
                receiver_process.terminate()
                while receiver_process.is_alive():
                    pass
                offline_time = random.randrange(2100,8700)/100
                print('[test]: killing receiver for about {} seconds'.format(offline_time))
                status['offline_timestamp'] = time.time()
                status['current_job_id'] = None
                status['check_recovery'] = True


            if (not receiver_process.is_alive()
                    and time.time() - status['offline_timestamp'] > offline_time):
                receiver = MoonbounceReceiver(receiver_api_client)
                receiver_process = Process(target=receiver.run)
                processes_dict['rx'] = receiver_process
                receiver_process.start()
                while not receiver_process.is_alive():
                    pass
                print('[test]: respawn receiver')
                last_respawn_time = time.time()
                status['recover_start_timestamp'] = time.time()

            '''transmitter'''
            if (random.random() < 0.4285714285714286 # 0.3/0.7
                    and transmitter_process.is_alive()
                    and receiver_process.is_alive()
                    and server_process.is_alive()
                    and time.time() - last_respawn_time > min_running_interval):
                transmitter_process.terminate()
                while transmitter_process.is_alive():
                    pass
                offline_time = random.randrange(2240,8330)/100
                print('[test]: killing transmitter for about {} seconds'.format(offline_time))
                status['offline_timestamp'] = time.time()
                status['current_job_id'] = None
                status['check_recovery'] = True


            if (not transmitter_process.is_alive()
                    and time.time() - status['offline_timestamp'] > offline_time):
                transmitter = MoonbounceTransmitter(transmitter_api_client)
                transmitter_process = Process(target=transmitter.run)
                processes_dict['tx'] = transmitter_process
                transmitter_process.start()
                while not transmitter_process.is_alive():
                    pass
                print('[test]: respawn transmitter')
                last_respawn_time = time.time()
                status['recover_start_timestamp'] = time.time()

            '''server'''
            if (random.random() < 0.75 # 0.3/((1-0.3)*(1-0.3/(1-0.3)))
                    and server_process.is_alive()
                    and transmitter_process.is_alive()
                    and receiver_process.is_alive()
                    and time.time() - last_respawn_time > min_running_interval):
                server_process.terminate()
                while server_process.is_alive():
                    pass
                offline_time = random.randrange(2090,8110)/100
                print('[test]: killing server for about {} seconds'.format(offline_time))
                status['offline_timestamp'] = time.time()
                status['current_job_id'] = None
                status['check_recovery'] = True


            if (not server_process.is_alive()
                    and time.time() - status['offline_timestamp'] > offline_time):
                server = MoonbounceServer()
                server_process = Process(target=server.run)
                processes_dict['srv'] = server_process
                server_process.start()
                while not server_process.is_alive():
                    pass
                print('[test]: respawn server')
                last_respawn_time = time.time()
                status['recover_start_timestamp'] = time.time()


            '''check_recovery'''
            if status['check_recovery'] is True and server_process.is_alive():
                current_job_id = await self.get_current_job_id(test_api_client)
                print(f"[test]: job_change_counter: {status['job_change_counter']}, "
                        f"current_job_id: {status['current_job_id']}")
                #assertIsNone(current_job_id, msg = "no current id was found")
                if current_job_id is None:
                    print('[test]: no current id was found')
                    sys.ext(-1)
                if status['current_job_id'] == None:
                    status['current_job_id'] = current_job_id
                elif status['current_job_id'] != current_job_id:
                    if status['job_change_counter'] >= 2:
                        recovery_time = (time.time()
                            - status['recover_start_timestamp'])
                        self.assertLess(recovery_time, min_running_interval)
                        print(f"[test]: fully recovered in {recovery_time:.2f} seconds.")
                        result = unittest.TestResult()
                        unittest.registerResult(result)
                        status['current_job_id'] = None
                        status['job_change_counter'] = 0
                        status['check_recovery'] = False
                    else:
                        status['current_job_id'] = current_job_id
                        status['job_change_counter'] += 1

            await asyncio.sleep(random.randrange(300,700)/100)

if __name__ == '__main__':
    unittest.main(failfast=True)
