# Moonbounce clients

This is a repository for the transmitter and receiver of the Moonbounce project.


## Dependence

- httpx
- flask (optional, only for the mock server)

## Usage

There is a full example in `test/main_test.py`.

Create a config dictionary.
```
receiver_client_cofnig = {
            'host_url': 'http://127.0.0.1',
            'api_route': 'api',
            'port': 5000,
            'api_version': '1.0.1',
            'session_id': 0,
            'client_type': 'receiver',
            'client_id': '[rx_uuid]'
}
```
Instantiate a client API object.
```
receiver_api_client = MoonbounceClientAPI(receiver_client_cofnig)
```
Instantiate a client with the API object.
```
receiver = MoonbounceReceiver(receiver_api_client)
```
Start the client
```
receiver.run()
```

