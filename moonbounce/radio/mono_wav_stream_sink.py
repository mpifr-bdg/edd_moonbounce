
import numpy as np
from gnuradio import gr
import wave
from threading import Event

class MonoWavStreamSink(gr.sync_block):

    def __init__(self):
        gr.sync_block.__init__(
            self,
            name='Mono Wav Stream Sink',
            in_sig=[np.float32],
            out_sig=None
        )

        self.record_active = Event()
        self.file_writing_stop = Event()

    def start_recording(self, filename, sample_width=2, sample_rate=44100):
        self.filename = filename
        self.file_handler = wave.open(filename, 'w')
        self.file_handler.setnchannels(1)
        self.file_handler.setsampwidth(sample_width)
        self.file_handler.setframerate(sample_rate)
        self.record_active.set()

    def stop_recording(self):
        self.record_active.clear()
        self.file_writing_stop.wait(30)
        if self.file_writing_stop.is_set():
            self.file_handler.close()
        else:
            print('file are still being written')
        return self.filename

    def work(self, input_items, output_items):

        if self.record_active.is_set():
            self.file_writing_stop.clear()
            audio = np.array(input_items[0])
            # Convert to (little-endian) 16 bit integers.
            audio = (audio * (2 ** 15 - 1)).astype("<h")
            self.file_handler.writeframes(audio.tobytes())
            self.file_writing_stop.set()
        else:
            # do nothing
            pass

        return len(input_items[0])
