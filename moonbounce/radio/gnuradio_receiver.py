#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# SPDX-License-Identifier: GPL-3.0
#
# GNU Radio Python Flow Graph
# Title: AM_SSB_SC_RX
# Author: wchen
# GNU Radio version: 3.8.1.0

from gnuradio import blocks
from gnuradio import gr
from gnuradio.filter import firdes
import sys
import signal
from argparse import ArgumentParser
from gnuradio.eng_arg import eng_float, intx
from gnuradio import eng_notation
import dual_pol_split_spead_source
import on_demand_dual_file_sink
import realfft
import spectrum_sender
import tagged_stream_to_vector

class gnuradio_receiver(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "AM_SSB_SC_RX")

        ##################################################
        # Variables
        ##################################################
        self.samp_rate = samp_rate = int(500e3)
        self.fft_size = fft_size = int(2**19)

        ##################################################
        # Blocks
        ##################################################
        self.tagged_stream_to_vector = tagged_stream_to_vector.tagged_stream_to_vector(num_items=fft_size)
        self.spectrum_sender = spectrum_sender.SpectrumSender(fft_size=fft_size)
        self.realfft = realfft.RealFFT(fft_size=fft_size)
        self.on_demand_dual_file_sink = on_demand_dual_file_sink.OnDemandDualFileSink()
        self.dual_pol_split_spead_source = dual_pol_split_spead_source.DualPolSplitSpeadSource()
        self.blocks_moving_average_xx_0 = blocks.moving_average_ff(3, 1, 4000, fft_size//2)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.blocks_moving_average_xx_0, 0), (self.spectrum_sender, 0))
        self.connect((self.dual_pol_split_spead_source, 1), (self.on_demand_dual_file_sink, 1))
        self.connect((self.dual_pol_split_spead_source, 0), (self.on_demand_dual_file_sink, 0))
        self.connect((self.dual_pol_split_spead_source, 0), (self.tagged_stream_to_vector, 0))
        self.connect((self.realfft, 0), (self.blocks_moving_average_xx_0, 0))
        self.connect((self.tagged_stream_to_vector, 0), (self.realfft, 0))

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate

    def get_fft_size(self):
        return self.fft_size

    def set_fft_size(self, fft_size):
        self.fft_size = fft_size
        self.realfft.fft_size = self.fft_size



def main(top_block_cls=gnuradio_receiver, options=None):
    tb = top_block_cls()

    def sig_handler(sig=None, frame=None):
        tb.stop()
        tb.wait()
        sys.exit(0)

    signal.signal(signal.SIGINT, sig_handler)
    signal.signal(signal.SIGTERM, sig_handler)

    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
