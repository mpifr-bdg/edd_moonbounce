import numpy as np
from gnuradio import gr
import os, errno, time, json
from threading import Event
import queue
import logging
from mpikat.utils.spead_capture import SpeadCapture
from mpikat.core.data_stream import convert48_64
from dbbc.utils import data_streams as ds
import pmt

_LOG = logging.getLogger('mpikat.moonbounce.radio.dual_pol_split_spead_source')

class DualPolSplitSpeadSource(gr.sync_block):

    def __init__(self):
        gr.sync_block.__init__(
            self,
            name='Dual Pol Split Spead Source',
            in_sig=None,
            out_sig=[np.float32, np.float32]
        )

        # self.output_queue = queue.Queue(self.queue_size)
        self._stop_event = Event()
        self.capture_active = False
        self.sync_time = None
        self.heap_dict = {}
        self.stat = {}
        self.stat['output'] = [0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.stat['p_queue'] = [0, 0]
        self.stat['filling'] = 0
        self.stat_queue_depth_history = []
        self.stat_output_buffer_size_history = []
        self.stat['heap_lost_history'] = [[0, 0]]

    def heap_handling(self, items):

        timestamp = convert48_64(items["timestamp"].value)
        pol_index = 0 if convert48_64(items["polarization"].value) == 0 else 1

        if timestamp in self.heap_dict:
            #pol_index = 0 if convert48_64(items["polarization"].value) == 0 else 1
            try:
                self.heap_dict[timestamp][pol_index] = items["data"].value
            except KeyError:
                _LOG.error('key {} does not exist anymore for pol {}'.format(
                            timestamp, pol_index))
        elif self.priority_queue.qsize() > 0 and self.priority_queue.queue[0] > timestamp:
            _LOG.error(f'reject old data from pol: {pol_index} {self.priority_queue.qsize()}')
        elif self.priority_queue.qsize() == 0 or self.priority_queue.queue[0] < timestamp:
            new_received_heap = [items["data"].value, items["data"].value]
            #new_received_heap = self.empty_heap
            #new_received_heap[pol_index] = items["data"].value
            self.heap_dict[timestamp] = new_received_heap
            self.priority_queue.put(timestamp)

    def get_first_sample_time(self, sample_count):
        seconds = sample_count/self.sampling_rate + self.sync_time
        #print(sample_count, self.sampling_rate, self.sync_time, seconds, seconds*10e9)
        return seconds*1e9


    def configure(self, spead_config, input_streams, command_queue):
        self.heap_size = 2048
        self.queue_size = 2048*4
        self.min_num_queue_item = 512 + 8192 // self.heap_size

        self.spead_config = spead_config
        self.input_streams = input_streams
        self.priority_queue = queue.PriorityQueue(self.queue_size)
        self.fake_ring_buffer = self.FakeRingBuffer(self.priority_queue,
                self.heap_dict, self.heap_size, self.stat)
        self.capture_thread = None
        self.empty_heap = np.zeros((2, self.heap_size))
        self.command_queue = command_queue
        self.sync_time = input_streams[0]['sync_time']
        self.sampling_rate = input_streams[0]['sample_rate']

    def start_capture(self):
        if self.spead_config is None:
            raise Exception("spead config should not be None")

        spead_handler = ds.DDCSpeadHandler()
        self.capture_thread = SpeadCapture(
            self.input_streams, spead_handler,
            self.heap_handling, self.spead_config
        )
        self.capture_thread.start()
        self.capture_active = True


        _LOG.info(f"sync time is: {self.sync_time}")
        #_LOG.info(("heap time is: {}".format(self.get_heap_time()))

    def stop_capture(self):
        """
        Stop the capture thread
        """
        _LOG.info('try to stop the spead reading thread')
        self._stop_event.set()
        if self.capture_thread is not None and self.capture_thread.is_alive():
            self.capture_thread.stop()
            self.capture_thread.join(10)
        _LOG.info('spead reading thread stopped.')

    def prepare_to_exit(self):
        _LOG.info('the size of the priority buffer is: {}'
                .format(self.priority_queue.qsize()))
        # del self.output_queue
        del self.priority_queue
        self.capture_active = False
        heap_lost_history = self.stat.pop('heap_lost_history')
        _LOG.info(self.stat)
        '''
        np.save("queue_depth_history", self.stat_queue_depth_history)
        np.save("output_buffer_size_history", self.stat_output_buffer_size_history)
        np.save("stat_heap_lost_history", heap_lost_history[1:])
        '''

    def check_command_queue(self):
        while not self.command_queue.empty():
            command = self.command_queue.get_nowait()
            if command[1] == 'stop':
                _LOG.info('receive stop command in spead source block')
                self.stop_capture()

    def work(self, input_items, output_items):

        self.check_command_queue()

        if self._stop_event.is_set():
            _LOG.info('preparing the stats.')
            self.prepare_to_exit()
            _LOG.info('trigger the stop event of gnuradio')
            return -1

        if not self.capture_active:
            self.start_capture()
            self.capture_active = True

        current_output_buffer_size = len(output_items[0])

        timestamp = time.time_ns() // 1000000
        self.stat_queue_depth_history.append([timestamp, self.priority_queue.qsize()])
        self.stat_output_buffer_size_history.append([timestamp, current_output_buffer_size])

        while self.priority_queue.qsize() < self.min_num_queue_item:
            self.check_command_queue()
            if self._stop_event.is_set():
                self.prepare_to_exit()
                return -1

        if self.priority_queue.qsize() == self.queue_size:
            self.stat['p_queue'][0] += 1
        else:
            self.stat['p_queue'][1] += 1

        self.fake_ring_buffer.write_to_buffer(current_output_buffer_size, output_items)
        total_sample_count = self.fake_ring_buffer.get_sample_count()

        first_sample_count = total_sample_count - current_output_buffer_size
        self.add_item_tag(0, self.nitems_written(0),
              pmt.intern("spead_timestamp"),
              pmt.from_long(int(first_sample_count))
        )

        return current_output_buffer_size

    class FakeRingBuffer():
        '''
        fake ring buffer
        '''
        def __init__(self, priority_queue, heap_dict, heap_size, stat):

            self.queue = priority_queue
            self.partial_heap = [[], []]
            self.heap_dict = heap_dict
            self.heap_size = heap_size
            self.current_heap_count = 0
            self.stat = stat
            self.last_heap = np.zeros(heap_size)

        def get_new_heap(self):

            if self.queue.queue[0] == 0 or self.current_heap_count == 0:
                self.current_heap_count = self.queue.get_nowait()
                new_heap = self.heap_dict[self.current_heap_count]
                self.last_heap = new_heap
                del self.heap_dict[self.current_heap_count]
                return new_heap

            while self.queue.queue[0] <= self.current_heap_count:
                """descard the pass heap"""
                pass_heap_count = self.queue.get_nowait()
                del self.heap_dict[pass_heap_count]
            time_offset = self.queue.queue[0] - self.current_heap_count
            if time_offset > self.heap_size:
                if time_offset > 100 * self.heap_size:
                    _LOG.error('large number of heaps are lost, jump to the latest.')
                    self.current_heap_count = self.queue.get_nowait()
                    new_heap = self.heap_dict[self.current_heap_count]
                    self.last_heap = new_heap
                    del self.heap_dict[self.current_heap_count]
                else:
                    """fill a missing heap"""
                    new_heap = self.last_heap
                    self.current_heap_count += self.heap_size
                    self.stat['filling'] += 1
                    timestamp = time.time_ns() // 1000
                    self.stat['heap_lost_history'].append([timestamp,
                            self.stat['heap_lost_history'][-1][1] + 1])
            elif time_offset == self.heap_size:
                """get a expected heap"""
                self.current_heap_count = self.queue.get_nowait()
                new_heap = self.heap_dict[self.current_heap_count]
                self.last_heap = new_heap
                del self.heap_dict[self.current_heap_count]
            else:
                """get a heap with abnormal size"""
                new_heap = self.last_heap
                self.current_heap_count += self.heap_size
                self.stat['filling'] += 1

            return new_heap

        def get_sample_count(self):
            partial_heap_length = len(self.partial_heap[0])
            sample_count = self.current_heap_count + self.heap_size - partial_heap_length

            return sample_count

        def write_to_buffer(self, buffer_length, output_buffer):

            partial_heap_length = len(self.partial_heap[0])
            if partial_heap_length > buffer_length:
                output_buffer[0][:] = self.partial_heap[0][:buffer_length]
                output_buffer[1][:] = self.partial_heap[1][:buffer_length]
                self.partial_heap[0] = self.partial_heap[0][buffer_length:]
                self.partial_heap[1] = self.partial_heap[1][buffer_length:]
                output_slot = -1
                output_slot_remain = 0
            else:
                output_buffer[0][:partial_heap_length] = self.partial_heap[0]
                output_buffer[1][:partial_heap_length] = self.partial_heap[1]
                self.partial_heap = [[], []]
                output_slot = (buffer_length -
                            partial_heap_length) // self.heap_size
                output_slot_remain = (buffer_length  -
                            partial_heap_length) % self.heap_size

            self.stat['output'][output_slot] += 1
            output_index = 0
            while output_index < output_slot:
                new_heap = self.get_new_heap()
                start_index = partial_heap_length + output_index * self.heap_size
                end_index = partial_heap_length + (output_index + 1) * self.heap_size
                output_buffer[0][start_index:end_index] = new_heap[0]
                output_buffer[1][start_index:end_index] = new_heap[1]
                output_index += 1
            if output_slot_remain > 0:
                new_heap = self.get_new_heap()
                start_index = partial_heap_length + output_slot * self.heap_size
                end_index = start_index + output_slot_remain
                output_buffer[0][start_index:end_index] = new_heap[0][:output_slot_remain]
                output_buffer[1][start_index:end_index] = new_heap[1][:output_slot_remain]
                self.partial_heap = [new_heap[0][output_slot_remain:],
                                     new_heap[1][output_slot_remain:]]

