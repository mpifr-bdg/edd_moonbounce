import numpy as np
from gnuradio import gr
from threading import Event
from datetime import datetime
import logging
import pmt

_LOG = logging.getLogger('mpikat.moonbounce.radio.on_demand_dual_file_sink')

class OnDemandDualFileSink(gr.sync_block):

    def __init__(self):
        gr.sync_block.__init__(
            self,
            name='On-demand Dual File Sink',   # will show up in GRC
            in_sig=[np.float32, np.float32],
            out_sig=None
        )

        self.file_opened = False
        self.record_started = False
        self.opened_file_object_0 = None
        self.opened_file_object_1 = None
        self.filename = None

    def open_file(self, filename, timestamp):
        if self.file_opened is True:
            _LOG.error(f'{filename} is arealdy opened')
            return False
        time_str = datetime.now().isoformat()
        self.opened_file_object0 = open(f"{filename}_0_{timestamp}", 'wb')
        self.opened_file_object1 = open(f"{filename}_1_{timestamp}", 'wb')
        self.filename = filename
        self.file_opened = True

    def close_file(self):
        if self.record_started is True:
            self.opened_file_object0.close()
            self.opened_file_object1.close()
            self.file_opened = False
            return self.filename
        else:
            _LOG.error('the recording is not started')
            return None

    def configure(self, command_queue):
        self.command_queue = command_queue

    def get_timestamp(self, input_streams):
        input_buffer_length = len(input_streams[0])
        tags = self.get_tags_in_window(0, 0, input_buffer_length)
        for tag in tags:
            if pmt.to_python(tag.key) != 'spead_timestamp':
                continue

            sample_counter = pmt.to_python(tag.value)
            offset_to_first_sample = tag.offset - self.nitems_read(0)
            first_sample_timestamp = sample_counter - offset_to_first_sample
            break
        else:
            _LOG.error('no spead timestamp tag was found')
            return 0

        return first_sample_timestamp

    def check_command_queue(self):
        while not self.command_queue.empty():
            command = self.command_queue.get_nowait()
            if command[1] == 'attach':
                _LOG.info(f'attach file: {command[2]}')
                self.record_started = True
                self.filename = command[2]
                #self.open_file(command[2])
            elif command[1] == 'detach':
                _LOG.info(f'detach file: {self.filename}')
                self.close_file()
                self.record_started = False

    def work(self, input_items, output_items):
        self.check_command_queue()
        if self.record_started is True:
            if self.file_opened is False:
                timestamp = self.get_timestamp(input_items)
                self.open_file(self.filename, timestamp)

            self.opened_file_object0.write(input_items[0])
            self.opened_file_object1.write(input_items[1])
        return len(input_items[0])
