import numpy as np
from gnuradio import gr
import numpy
import logging

_LOG = logging.getLogger('mpikat.moonbounce.radio.realfft')

class RealFFT(gr.sync_block):

    def __init__(self, fft_size=524288):
        gr.sync_block.__init__(
            self,
            name='Real FFT',
            in_sig=[(np.float32, fft_size)],
            out_sig=[(np.float32, int(fft_size//2))]
        )

        self.fft_size = fft_size
        #self.averager = self.Averager(10)
        
    def calc_real_fft(self, data, fft_size):
        return np.fft.rfft(data)
     
    '''    
    class Averager:
        
        def __init__(self, averaging_factor):
            self.buffer = []
            self.ready = False
            self.averaging_factor = averaging_factor
            
        def append(self, new_item):                
            self.buffer.append(new_item)
            if self.ready is False:
                if len(self.buffer) == self.averaging_factor:
                    print('average ready')
                    self.sum = np.sum(self.buffer, axis = 0)         
                    self.ready = True
            else:
                oldest_item = self.buffer.pop(0)
                self.sum += new_item  
                self.sum -= oldest_item  
                
                    
        def get_average(self):            
            return self.sum / self.averaging_factor 
    '''
    
    def work(self, input_items, output_items):
        
        ffted = self.calc_real_fft(input_items[0][0], self.fft_size)
        output_items[0][0][:] = np.square(np.abs(ffted))[:-1]
        
        '''
        ffted = self.calc_real_fft(input_items[0][0], self.fft_size)   
        self.averager.append(np.square(np.abs(ffted)))
        if self.averager.ready:
            averaged = self.averager.get_average()   
            output_items[0][0][:] = averaged[:-1]
        else:
            return 0
        '''
                
        return len(input_items[0])
