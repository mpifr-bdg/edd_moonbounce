
import numpy as np
from gnuradio import gr
from threading import Thread, Event
import collections
import time
import logging

_LOG = logging.getLogger('mpikat.moonbounce.polarization_combination')

class PolarizationCombine(gr.sync_block): 

    def __init__(self):  
        gr.sync_block.__init__(
            self,
            name='Polarization Combination', 
            in_sig=[np.float32, np.float32],
            out_sig=[np.float32]
        )
    
        circular_buffer_size = 1000
        eigen_calc_interval = 600
        self.buffer_manager = self.CircularBufferManager(circular_buffer_size)
        self.eigen_vector_ready = False
        self.eigen_vector = None
        self.eigen_calc_thread = Thread(
            target=self.calculate_eigen_vector_intervally, args=(eigen_calc_interval,))
        self.exit = Event()
        
        
    def calculate_eigen_vector(self):
        if self.buffer_manager.ready is False:
            _LOG.error('buffer_manager is not ready')
            return
         
        data = self.buffer_manager.get_buffer()    
        pol_covariance = np.cov(data)
        self.eigen_vector, self.eigen_value, v = np.linalg.svd(pol_covariance)
        if self.eigen_vector_ready is not True:
            self.eigen_vector_ready = True
        
    def calculate_eigen_vector_intervally(self, interval):
        
        while not self.exit.is_set():
            _LOG.info('eigen vector calculation.')
            self.calculate_eigen_vector()
            self.exit.wait(timeout=interval)
        _LOG.info('eigen_calc_thread stopped')
        

    def sum_polarization(self, pol0, pol1):
        '''
        combined = np.sum([np.square(pol0) * np.sign(pol0), 
                           np.square(pol1) * np.sign(pol1)], axis=0)
        '''
        combined = np.sum([pol0, pol1], axis=0)
            
        return combined
        
    def rotate_polarization(self, pol0, pol1):        
        
        rotated_data = np.dot(self.eigen_vector, [pol0, pol1])

        rotated_sorted_data = rotated_data[np.argsort(self.eigen_value)[::-1]]
                   
        return rotated_sorted_data

    def work(self, input_items, output_items):
        
        '''
        if (not self.exit.is_set()) and (not self.eigen_calc_thread.is_alive()):
            self.eigen_calc_thread.start()
        '''

        self.buffer_manager.is_free.wait()
        self.buffer_manager.append([input_items[0], input_items[1]])
        if self.buffer_manager.ready is True and self.eigen_vector_ready is True:
            output_items[0][:] = self.rotate_polarization(input_items[0], input_items[1])[0]
        else:
            #output_items[0][:] = input_items[0]
            output_items[0][:] = self.sum_polarization(input_items[0], input_items[1])
            
        return len(output_items[0])
        
    def stop(self):
        _LOG.info('receive stop signal')
        self.exit.set()
        
        return True        
        
        
    class CircularBufferManager:
        
        def __init__(self, max_size):
            self.buffer = collections.deque(maxlen=max_size)
            self.startup_count = 0
            self.ready = False
            self.max_size = max_size
            self.is_free = Event()
            self.is_free.set()
            
        def append(self, item):
            self.buffer.append(item)
            if self.ready is False:
                self.startup_count += 1
                if self.startup_count == self.max_size:
                    _LOG.info('buffer ready')
                    self.ready = True
                    
        def get_buffer(self):
            self.is_free.clear()
            stacked_data = np.hstack(self.buffer)
            self.is_free.set()
            return stacked_data      
