import numpy as np
from gnuradio import gr
import pmt
import logging

_LOG = logging.getLogger('mpikat.moonbounce.radio.spectrum_sender')

class SpectrumSender(gr.sync_block):

    def __init__(self, fft_size=524288):
        gr.sync_block.__init__(
            self,
            name='Spectrum Sender',
            in_sig=[(np.float32, int(fft_size//2))],
            out_sig=None
        )

        self.queue = None
        self.window = None

    def configure(self, queue, window=None):
        self.queue = queue
        self.window = window

    def work(self, input_items, output_items):
        if self.queue != None and not self.queue.full():
            tags = self.get_tags_in_window(0, 0, len(input_items[0]))
            key = None
            for tag in tags:
                key = pmt.to_python(tag.key)
                value = pmt.to_python(tag.value)
                if key == 'first_sample_timestamp':
                    break
            if key is not None:
                try:
                    if self.window:
                        self.queue.put_nowait([key, value, input_items[0][0][
                                self.window[0]:self.window[1]]])
                    else:
                        self.queue.put_nowait([key, value, input_items[0][0]])
                except Exception as e:
                    _LOG.error(e)
        else:
            return 0
        return len(input_items[0])
