import numpy as np
from gnuradio import gr
import pmt


class tagged_stream_to_vector(gr.decim_block):
    def __init__(self, num_items=2**19):
        gr.decim_block.__init__(self,
            name="Tagged Stream to vector",
            in_sig=[np.float32],
            out_sig=[(np.float32, int(num_items))],
            decim = num_items)
        self.set_relative_rate(1.0/num_items)
        self.decimation = num_items

    def work(self, input_items, output_items):

        input_buffer_length = len(input_items[0])
        tags = self.get_tags_in_window(0, 0, input_buffer_length)
        for tag in tags:
            if pmt.to_python(tag.key) != 'spead_timestamp':
                continue

            '''
            tag_dict = pmt.to_pmt({'offset': tag.offset,
                                    'nitems_read': self.nitems_read(0),
                                    'counter':pmt.to_python(tag.value)})
            '''
            sample_counter = pmt.to_python(tag.value)
            offset_to_first_sample = tag.offset - self.nitems_read(0)
            first_sample_timestamp = sample_counter - offset_to_first_sample
            self.add_item_tag(0, self.nitems_written(0),
                  pmt.intern("first_sample_timestamp"),
                  pmt.from_long(int(first_sample_timestamp))
            )
            break

        output_items[0][0][:] = input_items[0]
        return len(output_items[0])
