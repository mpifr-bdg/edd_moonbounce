"""
Author: Weiwei Chen
Mail: wchen@mpifr-bonn.mpg.de
Author: Niclas Esser
Mail: nesser@mpifr-bonn.mpg.de

Description:

Example pipeline on how to subscribe to a DigitalDownConverter. The pipeline expectes data
in the DigitalDownConverter:1-format.
"""
# pylint: disable=duplicate-code
# python natives
import asyncio
import json
# third party
from aiokatcp.sensor import Sensor
from aiokatcp.server import RequestContext
# EDD libs
from mpikat.core.edd_pipeline_aio import EDDPipeline, launchPipelineServer, state_change
from mpikat.core.data_stream import convert48_64
from mpikat.utils import numa
from mpikat.utils.spead_capture import SpeadCapture
import mpikat.core.logger as logging

from dbbc.utils import data_streams as ds

from moonbounce.client.receiver import MoonbounceReceiver
from moonbounce.client.moonbounce_api_client import MoonbounceAPIClient
# from moonbounce import version

_LOG = logging.getLogger("mpikat.moonbounce.pipelines.moonbounce")

"""
Define a default configuration in a python dicitionary.
The dicitionary should provide all required information to operate the pipeline
"""
_DEFAULT_CONFIG = {

    "id": "RxMoonbounce",
    "type": "RxMoonbounce",
    "n_heaps_per_block": 16384,
    "input_data_streams":
    [
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.124",
            "port": "8125"
        },
        {
            "format": "DigitalDownConverter:1",
            "ip": "239.0.255.128",
            "port": "8125"
        },
    ],
    "spead_config": SpeadCapture.DEFAULT_CONFIG,
    # User params
    "output_type": 'disk',
    "file_size":0,
    "output_directory": "/mnt/",
    # Debug / expert params
    "log_level": "debug"
}

receiver_client_config = {
                'host_url': 'https://moonbounce.mpifr-bonn.mpg.de',
                'key_cloak_url': 'https://login-moonbounce.mpifr-bonn.mpg.de',
                'api_route': 'api',
                'token_route': 'realms/moonbounce/protocol/openid-connect/token',
                'heartbeat_route': 'receiver/status',
                'spectrum_route': 'receiver/spectrum',
                'api_version': '0.0.1',
                'session_id': 0,
                'client_credential': {},
                'key_cloak_cert': '/src/edd_moonbounce/credentials/keycloak_cert.pem',
                'client_type': 'receiver',
                'client_id': '[rx_uuid]'}

class RxMoonbounce(EDDPipeline):
    """
    Description:
        The RxMoonbounce pipeline is an example pipeline on how to subscribe to
        DigitalDownconverter Streams. Hence the only accepted DATA_FORMATS is the
        DDCDataStreamFormat.
    """

    DATA_FORMATS = [ds.DDCDataStreamFormat.stream_meta]

    def __init__(self, ip: str, port: int):
        """
        Description:
            Construct the RxMoonbounce pipeline.

        Args:
            - ip (str): The ip of the KATCP server
            - port (int): The port of the KATCP server
        """
        # Call to the base class for constructing the pipeline
        super().__init__(ip, port, _DEFAULT_CONFIG)
        self.capture_thread = None
        #self.gnuradio_receiver = None
        self.receiver = None
        _LOG.info("RxMoonbounce pipeline instantiated")

    def setup_sensors(self):
        """
        Description:
            Setups KATCP sensors. You can define as many sensor as you like
        """
        # Call to the base class for setting up common sensors
        super().setup_sensors()
        # The aiokatcp.DeviceServer owns a set() of sensors (self.sensors)
        self.sensors.add(Sensor(str, "example-name",
            description="I am an example sensor"
        ))

    @state_change(target="configured", allowed=["idle"], intermediate="configuring")
    async def configure(self):
        """
        Configure the EDD VLBi pipeline

        """
        _LOG.info("Configuring RxMoonbounce processing")
        # Initialize the spead capturing
        # We use mpikat.utils.numa to get the IP address of the fastest
        # network interface card / network device
        nic_name, nic_desc = numa.getFastestNic()
        _LOG.info(
            "Capturing on interface %s, ip %s, speed %s Mbit/s",
            nic_name, nic_desc["ip"], nic_desc["speed"]
        )
        # The mpikat.utils.speadcapture.SpeadCapture class requires the NIC IP
        # and the number of CPU cores to ensure threaded capturing
        self._config["spead_config"]["interface_address"] = nic_desc["ip"]
        self._config["spead_config"]["numa_affinity"] = numa.getInfo()[nic_desc['node']]['cores']
        # Instantiate the a DDCSpeadHandler object to provide spead2 the necessary information
        # about the used spead format. Then we can instantiate a SpeadCapture object to handle
        # the spead heaps. The passed function 'self.heap_handling' is used to access the spead data.
        # This function is the 'user-space'-function and called whenever a spead heap is received
        _LOG.debug("Pipeline is configured")
        # _LOG.INFO(f"Moonbounce EDD Pipeline version: {version}")

        with open('/src/edd_moonbounce/credentials/credential', 'r') as credential_file:
            credential = json.load(credential_file)
        receiver_client_config["client_credential"] = credential["client_credential"]
        receiver_api_client = MoonbounceAPIClient(receiver_client_config)
        self.receiver = MoonbounceReceiver(receiver_api_client, self._config)
        self._configUpdated()


    @state_change(target="streaming", allowed=["configured"], intermediate="configured")
    async def capture_start(self):
        """
        @brief start streaming output
        """
        # Here we start the actual netowrk capturing of the spead heaps
        # self.capture_thread.start()
        #self.gnuradio_receiver.start()
        self.receiver.start()
        _LOG.debug("Started capturing")

    async def request_attach_file_sink(self, ctx: RequestContext,
            filename='/workspace/spead.input'):
        """
        @brief request a new katcp command: attach_file_sink
        """
        await self.attach_file_sink(filename)

    async def request_detach_file_sink(self, ctx: RequestContext):
        """
        @brief request a new katcp command: detech_file_sink
        """
        await self.detach_file_sink()

    async def request_spectrum_record_start(self, ctx: RequestContext):
        """
        @brief request a new katcp command: recording_start
        """
        await self.spectrum_record_start()

    async def request_spectrum_record_stop(self, ctx: RequestContext):
        """
        @brief request a new katcp command: recording_stop
        """
        await self.spectrum_record_stop()

    async def request_recording_start(self, ctx: RequestContext):
        """
        @brief request a new katcp command: recording_start
        """
        await self.recording_start()

    async def request_correct_freq_shift(self, ctx: RequestContext):
        """
        @brief request a new katcp command: correct_freq_shift
        """
        await self.correct_freq_shift()

    async def request_make_spectrum(self, ctx: RequestContext,
            start_freq = -22050, end_freq = 22050):
        """
        @brief request a new katcp command: make_spectrum
        """
        await self.make_spectrum(int(start_freq), int(end_freq))

    async def request_recording_stop(self, ctx: RequestContext):
        """
        @brief request a new katcp command: recording_stop
        """
        await self.recording_stop()

    async def attach_file_sink(self, filename_byte):
        """
        @brief open a file to save the data
        """
        filename = filename_byte.decode('utf-8')
        self.receiver.attach_file_sink(filename)

    async def detach_file_sink(self):
        """
        @brief close the opened file
        """
        self.receiver.detach_file_sink()

    async def spectrum_record_start(self):
        """
        @brief open a file to save spectrum with the start timestamp as filename
        """
        self.receiver.start_spectrum_recording()

    async def spectrum_record_stop(self):
        """
        @brief close the spectrum file
        """
        self.receiver.stop_spectrum_recording()

    async def correct_freq_shift(self):
        """
        @brief correct frequency shift by locating calibrating tones.
        """
        self.gnuradio_receiver.spectrum_reader.correct_freq_shift()

    async def make_spectrum(self, start_freq, end_freq):
        """
        @brief make spectrum
        """
        try:
            self.gnuradio_receiver.spectrum_reader.make_spectrum(
                    '/workspace/spectrum.png', [start_freq, end_freq])

        except Exception as e:
            print(e)

        _LOG.info("spectrum generated.")

    async def recording_start(self):
        """
        @brief start recording
        """
        self.gnuradio_receiver.mono_wav_stream_sink.start_recording(
                '/workspace/recording.wav')

        _LOG.info("Started recording")

    async def recording_stop(self):
        """
        @brief stop recording
        """
        wav_filename = self.gnuradio_receiver.mono_wav_stream_sink.stop_recording()
        _LOG.info(wav_filename)
        _LOG.info("Stopped recording")

    @state_change(target="idle", allowed=["streaming"], intermediate="streaming")
    async def capture_stop(self):
        """
        @brief Stop streaming of data
        """
        await self.deconfigure()

    @state_change(target="idle", intermediate="deconfiguring", error='panic')
    async def deconfigure(self):
        """
        @brief deconfigure the pipeline.
        """
        await self._destroy_capture()

    def heap_handling(self, items):
        """
        """
        # if items["timestamp"] > Rest.get("timestamp_start") and items["timestamp"] > Rest.get("timestamp_stop"):
        #     self.queue.fill(items["data"])
        _LOG.debug("Received heap with timestamp %s and channel_id %s",
            convert48_64(items["timestamp"].value),
            convert48_64(items["channel"].value)
        )

    async def _destroy_capture(self):
        """
        """
        if self.receiver is not None:
            await self.receiver.stop()
            _LOG.info("gnuradio receiver stopped")
        '''
        if self.gnuradio_receiver is not None:
            self.gnuradio_receiver.dual_pol_split_spead_source.stop_capture()
            self.gnuradio_receiver.stop()
            self.gnuradio_receiver.wait()
            self.gnuradio_receiver = None
        '''

if __name__ == "__main__":
    asyncio.run(launchPipelineServer(RxMoonbounce))
