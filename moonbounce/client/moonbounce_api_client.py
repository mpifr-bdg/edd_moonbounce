#!/usr/bin/env python3

import os
import sys
import time
import json
import httpx
import logging
import traceback

_LOG = logging.getLogger('mpikat.moonbounce.radio.moonbounce_api_client')

'''
logging.basicConfig(
    format="%(levelname)s [%(asctime)s] %(name)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
    level=logging.DEBUG
)
'''

DEFAULT_TIMEOUT = 10  # seconds

async def log_request(request):
    _LOG.debug(f"Request event hook: {request.method} "
              f"{request.url} - Waiting for response")


async def log_response(response):
    await response.aread()
    request = response.request
    _LOG.debug(f"Response event hook: {request.method} {request.url} - "
              f"Status {response.status_code}")
    _LOG.debug(f"Response payload: {response.text}")


async def raise_on_4xx_5xx(response):
    response.raise_for_status()


class MoonbounceAPIClient:
    """
    A Moonbounce WebAPI client.
    """

    def __init__(self, config: dict):
        """
        Instantiate a new client instance

        :param config:  A configuration dictionary
        """
        self._api_version = config['api_version']
        self._client_type = config['client_type']
        self._client_id = config['client_id']
        self.session_id = config['session_id']
        self._moonbounce_base_url = "{host_url}".format(
            **config)
        self._moonbounce_key_cloak_url = "{key_cloak_url}".format(
            **config)
        self._jobs_url = f"{config['api_route']}/jobs"
        self._heartbeat_url = f"/{config['api_route']}/{config['heartbeat_route']}"
        self._token_url = f"/{config['token_route']}"
        self._spectrum_url =  f"/{config['api_route']}/{config.get('spectrum_route', '')}"
        cert = config['key_cloak_cert']
        limits = httpx.Limits(max_connections=100)
        transport = httpx.AsyncHTTPTransport(retries=3, verify=cert)
        event_hooks = {
            'request': [log_request],
            'response': [log_response, raise_on_4xx_5xx]
        }


        self._session = httpx.AsyncClient(
            base_url=self._moonbounce_base_url,
            transport=transport,
            limits=limits,
            event_hooks=event_hooks,
            timeout=config.get("timeout", DEFAULT_TIMEOUT))

        self._key_cloak_session = httpx.AsyncClient(
            base_url=self._moonbounce_key_cloak_url,
            transport=transport,
            limits=limits,
            event_hooks=event_hooks,
            timeout=config.get("timeout", DEFAULT_TIMEOUT))

        self.tokens = {'access_expire_time': 0, 'refresh_expire_time': 0}
        self.client_credential = config['client_credential']

    def __repr__(self):
        return f"<MoonbounceAPIClient {self._client_type}:{self._client_id}>"

    async def shutdown(self):
        await self._session.aclose()
        await self._key_cloak_session.aclose()

    def _make_payload(self, params: dict = None) -> dict:
        if params is None:
            params = {}
        params.update(
            {'client_id': self._client_id,
             'api_version': self._api_version,
             'client': self._client_type})
        return params

    def _status_error_msg(self, message: str) -> str:
        caller = sys._getframe().f_back.f_code.co_name
        return f"{repr(self)} {caller} {message}"

    async def get_job(self, job_id: str = None) -> dict:
        """
        Get a job object from the API

        :param      job_id:  The job identifier

        :returns:   An API job object
        """
        if job_id is not None:
            url = f"{self._jobs_url}/{str(job_id)}"
        else:
            url = self._jobs_url
        try:
            response = await self._session.get(
                url, params=self._make_payload())
            if response.status_code == httpx.codes.OK:
                result = response.json()
                return result.get('jobs', [])
            else:
                _LOG.error(self._status_error_msg(
                    f"status code {response.status_code}"))
                return []
        except Exception as e:
            _LOG.error(self._status_error_msg(str(e)))
            return []

    async def report_job_state(self, job_id: str, params: dict) -> None:
        """
        Report the job state to the server

        :param      job_id:   The job identifier
        :param      message:  The message
        """
        try:
            response = await self._session.put(
                f"{self._jobs_url}/{str(job_id)}",
                json=json.dumps(self._make_payload(params)))
            if response.status_code != httpx.codes.OK:
                _LOG.error(self._status_error_msg(
                    f"status code {response.status_code}"))
        except Exception as e:
            _LOG.error(self._status_error_msg(str(e)))

    async def send_heartbeat(self, state: str) -> None:
        """
        Send heartbeat to webserver

        :param      state:  The state to report to the server
        """
        try:
            is_token_acquired = await self.check_token_expiration()
            if not is_token_acquired:
                _LOG.error('access token was not accquired')
                return
            response = await self._session.put(
                self._heartbeat_url,
                json=self._make_payload({'state': state}),
                headers = {'Authorization': self.tokens['Authorization']}
                )
            #if response.status_code != httpx.codes.OK:
            if response.status_code > 204:
                _LOG.error(self._status_error_msg(
                    f"status code {response.status_code}"))
        except Exception as e:
            _LOG.error(traceback.format_exc())
            _LOG.error(self._status_error_msg(str(e)))

    async def check_token_expiration(self) -> None:
        current_time = time.time()
        if current_time - self.tokens['access_expire_time'] >= -1:
            _LOG.info('acquiring new token')
            return await self.acquire_token()
        else:
            return True

    async def acquire_token(self) -> None:
        """
        acquire token from the server

        :param      credential:  the credential for authentication
        """
        credential = self.client_credential
        try:
            response = await self._key_cloak_session.post(
                self._token_url,
                data={'username': credential['username'],
                      'password': credential['password'],
                      'client_id': credential['client_id'],
                      'client_secret': credential['client_secret'],
                      'grant_type': 'password'},
                      headers = {'Content-Type': 'application/x-www-form-urlencoded'})

            if response.status_code != httpx.codes.OK:
                _LOG.error(self._status_error_msg(
                    f"status code {response.status_code}"))
                return False
            else:
                token_dict = response.json()
                current_time = time.time()
                self.tokens = {"access_token": token_dict['access_token'],
                               "refresh_token": token_dict['refresh_token'],
                               "Authorization": f"Bearer {token_dict['access_token']}"
                }
                self.tokens['access_expire_time'] = token_dict[
                            'expires_in'] + current_time
                self.tokens['refresh_expire_time'] = token_dict[
                            'refresh_expires_in'] + current_time
                _LOG.debug('[receiver]: access token acquired')
                return True

        except Exception as e:
            _LOG.error(self._status_error_msg(str(e)))

    async def send_multipart_data(self, message:dict, binary: bytes) -> None:
        try:
            is_token_acquired = await self.check_token_expiration()
            #print(is_token_acquired)
            if not is_token_acquired:
                _LOG.error('access token was not accquired')
                return
            #print('posting',self._spectrum_url)
            response = await self._session.post(
                self._spectrum_url, data=message, files={'file': binary},
                headers = {"Authorization": self.tokens['Authorization']}
                )
            #if response.status_code != httpx.codes.OK:
            if response.status_code > 204:
                _LOG.error(self._status_error_msg(
                    f"status code {response.status_code}"))
                return False
            else:
                return True
        except Exception as e:
            _LOG.error(self._status_error_msg(str(e)))

