#!/usr/bin/env python3

import time
import asyncio
from multiprocessing import Process, Manager
import logging
import traceback

from moonbounce.radio.gnuradio_receiver import gnuradio_receiver
_LOG = logging.getLogger("mpikat.moonbounce.client.receiver")

class MoonbounceReceiver:
    '''
    Controller for the moonbounce receiver
    '''

    def __init__(self, client_api, config):

        # self.gnuradio = gnuradio_moonbounce_receiver()
        # self.gnuradio.spead_source.configure(spead_config, input_streams)
        self.stop_event = False

        self.background_task = {}
        self.job_polling_interval = 1
        self.heartbeat_interval = 3
        self.spectrum_interval = 1

        self.receiver_id = '[rx_uuid]'
        self.receiver_current_job_id = None
        self.transmission_start_time = None
        self.transmission = None
        self.transmission_duration = None

        self.receiver_state_max_time = -1
        self.receiver_state_timestamp = 0
        self.rx_states = [
            'idle',
            'record_starting',
            'recording',
            'recording_stopped',
            'sending_audio_file',
            'audio_file_sent',
            'halt']
        rx_state_dict = dict([(key, i + 1)
                             for i, key in enumerate(self.rx_states)])
        self.receiver_state = type('rx_state', (object,), rx_state_dict)()
        self.receiver_current_state = self.receiver_state.idle

        self.client_api = client_api
        self.gnuradio_process = None
        self.multiproc_manager = Manager()
        self.gnuradio_spead_source_command_queue = self.multiproc_manager.Queue()
        self.gnuradio_spectrum_sender_data_queue = self.multiproc_manager.Queue(maxsize=100000)
        self.gnuradio_spead_file_sink_command_queue = self.multiproc_manager.Queue()
        self.config = config

        self.receiver_spectrum_sender_task = None
        self.heartbeat_task = None
        self.main_task = None

        self.spectrum_margin_bins = 1900
        self.central_frequency = 1295.95e6
        self.receiver_frequency = 95.95e6
        self.receiver_lower_band = 95.875e6
        self.spectrum_fft_size = 2**19
        self.spectrum_recording = False
        self.spectrum_file = None

    async def authentication(self):
        is_authenticated = await self.client_api.acquire_token()
        return is_authenticated
        '''
        if is_authenticated:
            await self.client_api.send_heartbeat("authenticated")
        '''

    def start(self):
        #asyncio.run(self.client_api.acquire_token())

        self.stop_event = False
        self.start_gnuradio_process()
        self.main_task = asyncio.create_task(self.initialization())

    async def stop(self):
        self.stop_event = True
        if self.receiver_spectrum_sender_task is not None:
            await self.receiver_spectrum_sender_task
        if self.heartbeat_task is not None:
            await self.heartbeat_task
        if self.main_task is not None:
            await self.main_task
        await self.stop_gnuradio_process()

    async def initialization(self):
        is_authenticated = await self.authentication()
        _LOG.info(f"authenticated: {is_authenticated}")
        if is_authenticated:
            self.receiver_spectrum_sender_task = asyncio.create_task(
                self.receiver_spectrum_sender_worker())
            self.heartbeat_task = asyncio.create_task(
                self.receiver_heartbeat_worker())


    def start_gnuradio_process(self):
        gnuradio_receiver_manager = gnuradio_receiver()
        gnuradio_receiver_manager.dual_pol_split_spead_source.configure(
            self.config["spead_config"], self.config["input_data_streams"],
            self.gnuradio_spead_source_command_queue)
        sample_rate = self.config["input_data_streams"][0]['sample_rate']
        frequency_offset = self.receiver_frequency - self.receiver_lower_band
        bandwidth = sample_rate / 2
        signal_bin = int(frequency_offset / bandwidth * self.spectrum_fft_size / 2)
        gnuradio_receiver_manager.spectrum_sender.configure(
            self.gnuradio_spectrum_sender_data_queue,
                [signal_bin - self.spectrum_margin_bins,
                 signal_bin + self.spectrum_margin_bins])
        gnuradio_receiver_manager.on_demand_dual_file_sink.configure(
           self.gnuradio_spead_file_sink_command_queue)
        #gnuradio_receiver_manager.spead_source.configure()
        self.gnuradio_process = Process(target=gnuradio_receiver_manager.run)
        self.gnuradio_process.start()

    async def stop_gnuradio_process(self):
        if self.gnuradio_process is None:
            _LOG.info('gnuradio was not started.')
            return
        self.gnuradio_spead_source_command_queue.put([0, 'stop'])
        while not self.gnuradio_spectrum_sender_data_queue.empty():
            self.gnuradio_spectrum_sender_data_queue.get_nowait()
            self.gnuradio_spectrum_sender_data_queue.task_done()

        sleep_count = 7
        _LOG.info('waiting for gnuradio process to stop.')
        while self.gnuradio_process.is_alive() and sleep_count > 0:
            sleep_count -= 1
            await asyncio.sleep(1)
        sleep_count = 30
        if not self.gnuradio_process.is_alive():
            _LOG.info('gnuradio process exited.')
            return
        else:
            _LOG.info('terminating gnuradio process.')
            self.gnuradio_process.terminate()
        while self.gnuradio_process.is_alive() and sleep_count > 0:
            sleep_count -= 1
            await asyncio.sleep(1)
        if self.gnuradio_process.is_alive():
            _LOG.info('killing the gnuradio process.')
            self.gnuradio_process.kill()
        else:
            _LOG.info('gnuradio process was terminated.')

    def attach_file_sink(self, filename):
        self.gnuradio_spead_file_sink_command_queue.put([0, 'attach', filename])

    def detach_file_sink(self):
        self.gnuradio_spead_file_sink_command_queue.put([0, 'detach'])

    def start_spectrum_recording(self):
        try:
            if self.spectrum_recording is True:
                _LOG.error('there is already a spectrum file opened')
                return False
            self.spectrum_recording = True
        except Exception as e:
            _LOG.error(traceback.format_exc())

    def stop_spectrum_recording(self):
        if self.spectrum_recording is False:
            _LOG.error('no spectrum file is opened')
            return False
        _LOG.info(f'closing file: {self.spectrum_file.name}')
        self.spectrum_recording = False
        self.spectrum_file.close()
        self.spectrum_file = None

    async def async_tasks(self):
        await asyncio.gather(
            self.receiver_polling_worker(),
            self.receiver_heartbeat_worker())

    def get_current_state(self):
        return self.receiver_current_state

    def receiver_state_change(self, new_state, max_time=20, force=False):
        if (self.receiver_current_state == self.receiver_state.halt
                and force is not True):
            return
        self.receiver_current_state = new_state
        self.receiver_state_max_time = max_time
        self.receiver_state_timestamp = time.time()

    def receiver_state_go_idle(self, force=False):
        self.receiver_state_change(self.receiver_state.idle, -1, force)
        self.receiver_current_job_id = None
        self.transmission_start_time = None
        self.transmission = None
        self.transmission_duration = None

    def set_state_timeout(self, max_time):
        self.receiver_state_max_time = max_time
        self.receiver_state_timestamp = time.time()

    async def receiver_start_recording(self):
        await asyncio.sleep(3)
        return True

    async def receiver_stop_recording(self):
        await asyncio.sleep(2)
        return True

    async def receiver_send_audio(self, uri):
        await asyncio.sleep(3)
        self.receiver_state_change(self.receiver_state.audio_file_sent, 10)

    async def receiver_report_to_server(self, message, job_id):
        await self.client_api.report_job_state(job_id, message)

    async def receiver_interprete_job(self, jobs):
        for job in jobs:
            if job['session'] != self.client_api.session_id:
                continue
            if 'receiver' not in job['clients']:
                continue

            if job['action'] == 'start':
                self.receiver_state_go_idle(force=True)
                await self.receiver_report_to_server({}, job['id'])
                continue
            elif (job['action'] == 'halt' and self.receiver_current_state
                    != self.receiver_state.halt):
                self.receiver_state_change(self.receiver_state.halt, -1)
                await self.receiver_report_to_server({}, job['id'])

            if self.receiver_current_state == self.receiver_state.halt:
                return

            if (job['job_state'] != 'started'
                or job['receiver_completed'] is True
                    or job['receiver_error'] != ''):
                continue

            if (self.receiver_current_job_id is not None
                    and self.receiver_current_job_id != job['id']):
                continue

            if job['action'] == 'new_transmission':
                if self.receiver_current_state == self.receiver_state.idle:
                    await self.state_idle_director(job)

                elif self.receiver_current_state == self.receiver_state.recording:
                    await self.state_recording_director(job)

                elif self.receiver_current_state == self.receiver_state.audio_file_sent:
                    await self.state_audio_file_sent_director(job)

    async def state_idle_director(self, job):

        if job['transmission_end_time'] != '':
            _LOG('[receiver]: transmission has already ended')
            await self.receiver_report_to_server({
                'updates': {'receiver_state': 'idle',
                            "receiver_error": {'type': 'transmission was ended',
                                               'state': 'idle'}}}, job['id'])
            return

        ack_task = asyncio.create_task(self.receiver_report_to_server({
            'updates': {"receiver_state": "record_starting"}}, job['id']))
        self.receiver_state_change(
            self.receiver_state.record_starting)
        self.receiver_current_job_id = job['id']
        # self.receiver_job.id = job['id']
        is_record_started = await self.receiver_start_recording()
        await ack_task
        if is_record_started is True:
            await self.receiver_report_to_server({
                'updates': {"receiver_state": "recording"}}, job['id'])
            self.receiver_state_change(
                self.receiver_state.recording, 20)

    async def state_recording_director(self, job):

        if (job['transmitter_state'] == 'transmitting'
                and self.transmission_start_time is None):
            self.transmission_start_time = job['transmission_start_time']
            self.transmission_duration = job['transmission_duration']
            self.set_state_timeout(self.transmission_duration + 3)
        elif (job['transmitter_state'] == 'transmit_ended'
                or job['transmitter_completed'] is True):
            self.transmission_end_time = job['transmission_end_time']
            await self.receiver_stop_recording()
            self.receiver_state_change(
                self.receiver_state.recording_stopped, 2)
            await self.receiver_report_to_server({
                "updates": {"receiver_state": "recording_stopped"}}, job['id'])
            self.receiver_state_change(
                self.receiver_state.sending_audio_file, 20)
            await self.receiver_report_to_server({
                "updates": {"receiver_state": "sending_audio_file"}}, job['id'])
            audio_clip_sending_task = asyncio.create_task(
                self.receiver_send_audio('uri_to_audio'))
            self.background_task['audio_clip_sending'] = audio_clip_sending_task

    async def state_audio_file_sent_director(self, job):
        await self.background_task['audio_clip_sending']
        if self.receiver_current_state != self.receiver_state.halt:
            self.receiver_state_go_idle()
            await self.receiver_report_to_server({
                "updates": {"receiver_state": "idle",
                            "receiver_completed": True}}, job['id'])

    async def check_receiver_state_timeout(self):
        if self.receiver_state_max_time == -1:
            return False
        elif (time.time() - self.receiver_state_timestamp
              > self.receiver_state_max_time):
            _LOG.info('[receiver]: timeout.')
            current_state = self.rx_states[self.receiver_current_state - 1]
            await self.receiver_report_to_server({
                'updates': {'receiver_state': 'idle',
                            "receiver_error": {'type': 'timeout',
                                               'state': current_state
                                               }}}, self.receiver_current_job_id)
            self.receiver_state_go_idle()
            return True
        else:
            return False

    async def send_multipart_data(self, message, binary):
        return await self.client_api.send_multipart_data(message, binary)

    async def receiver_spectrum_sender_worker(self):
        sync_time = self.config["input_data_streams"][0]['sync_time']
        sample_rate = self.config["input_data_streams"][0]['sample_rate']
        spectrum_file_path = '/beegfsEDD/moonbounce/spectrum'
        bandwidth = sample_rate / 2
        spectrum_resolution = bandwidth / (self.spectrum_fft_size / 2 )
        spectrum_margin = spectrum_resolution * self.spectrum_margin_bins
        while not self.stop_event:
            if not self.gnuradio_spectrum_sender_data_queue.empty():
                message = self.gnuradio_spectrum_sender_data_queue.get_nowait()
                self.gnuradio_spectrum_sender_data_queue.task_done()
                spectrum_time = sync_time + message[1]/sample_rate
                spectrum_bytes = message[2].tobytes()
                is_sent_succeeded = await self.send_multipart_data(
                        {'timestamp': spectrum_time,
                        'frequance_range': [self.central_frequency - spectrum_margin,
                            self.central_frequency + spectrum_margin]},
                        spectrum_bytes)
                '''
                save spectrum to file for correlation
                '''

                if self.spectrum_recording is True:
                    if self.spectrum_file is None:
                        first_sample_time_ns = int(spectrum_time * 1e9)
                        filename = f"{spectrum_file_path}/{first_sample_time_ns}"
                        self.spectrum_file = open(filename, 'wb')
                        _LOG.info(f'opening file: {self.spectrum_file.name}')
                        os.chmod(filename, 0o666)
                    self.spectrum_file.write(spectrum_bytes)
                if is_sent_succeeded:
                    pass
                    #print('spectrum sent')
                else:
                    pass
                    #print('spectrum was not sent successfully.')

            await asyncio.sleep(self.spectrum_interval)
        if self.spectrum_file is not None:
            _LOG.info(f'closing file: {self.spectrum_file.name}')
            self.spectrum_file.close()
        _LOG.info('spectrum sender task stop')


    async def receiver_polling_worker(self):
        while True:
            try:
                await self.check_receiver_state_timeout()
                poll_result = await self.client_api.get_job(
                    self.receiver_current_job_id)
                await self.receiver_interprete_job(poll_result)
            except Exception as e:
                _LOG.error(e)
            await asyncio.sleep(self.job_polling_interval)

    async def receiver_heartbeat_worker(self):
        while not self.stop_event:
            try:
                await self.client_api.send_heartbeat(
                    self.rx_states[self.receiver_current_state - 1])
            except Exception as e:
                _LOG.error(traceback.format_exc())
            await asyncio.sleep(self.heartbeat_interval)
